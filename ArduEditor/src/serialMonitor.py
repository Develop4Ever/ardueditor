#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, time, serial # importo i moduli sys, time e serial
from PyQt4 import QtGui, QtCore # importo il toolkit grafico (PyQt4)

port = "/dev/ttyUSB0" # porta predefinita alla quale collegarsi
baudrate = 9600 # baudrate predefinito

class SerialMonitor(QtGui.QDialog):

    """classe principale: crea la grafica ed avvia il thread che riceve da seriale"""

    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.port = port
        self.rangePT = ["/dev/ttyUSB0", "/dev/ttyUSB1", "/dev/ttyUSB2"]
        self.baudrate = baudrate
        self.rangeBR = [300, 1200, 2400 , 4800 , 9600, 14400,
                        19200 , 28800 , 38400, 57600, 115200]
        self.setWindowTitle('Serial Monitor')
        self.textOutput = QtGui.QPlainTextEdit(self)
        self.textOutput.setReadOnly(True)
        self.sendText = QtGui.QLineEdit()
        self.sendText.returnPressed.connect(self.send)
        self.baudComboBox = QtGui.QComboBox()
        self.baudComboBox.currentIndexChanged.connect(self.changeBaud)
        for self.element in self.rangeBR:
            self.baudComboBox.addItem(str(self.element)+" baud")
        self.baudComboBox.setCurrentIndex(4)
        self.portComboBox = QtGui.QComboBox()
        self.portComboBox.currentIndexChanged.connect(self.changePort)
        for self.element in self.rangePT:
            self.portComboBox.addItem(self.element)
        self.portComboBox.setCurrentIndex(1)
        self.barLayout = QtGui.QHBoxLayout()
        self.barLayout.addWidget(self.sendText)
        self.barLayout.addWidget(self.baudComboBox)
        self.barLayout.addWidget(self.portComboBox)
        self.mainLayout = QtGui.QVBoxLayout(self)
        self.mainLayout.addWidget(self.textOutput)
        self.mainLayout.addLayout(self.barLayout)
        self.DataCollector = Monitor(self)
        self.connect(self.DataCollector, QtCore.SIGNAL("newText ( QString ) "), self.new)
        self.DataCollector.start()

    def send(self):
        """invia dati attraverso porta seriale (da testare)"""
        try:
            self.port = port
            self.baudrate = baudrate
            self.textToSend = self.sendText.text()
            self.Connection = serial.Serial(self.port, self.baudrate)
            self.Connection.write(self.textToSend)
        except serial.serialutil.SerialException:
            print "Impossibile collegarsi con la porta "+str(self.port)

    def changePort(self, index):
        """cambia la porta alla quale collegarsi"""
        global port
        port = self.rangePT[index]
        try:
            self.DataCollector.terminate()
            self.DataCollector = Monitor(self)
            self.connect(self.DataCollector, QtCore.SIGNAL("newText ( QString ) "), self.new)
            self.DataCollector.start()
        except AttributeError:
            pass

    def changeBaud(self, index):
        """cambia il baudrate del collegamento"""
        global baudrate
        baudrate = int(self.rangeBR[index])
        try:
            self.DataCollector.terminate()
            self.DataCollector = Monitor(self)
            self.connect(self.DataCollector, QtCore.SIGNAL("newText ( QString ) "), self.new)
            self.DataCollector.start()
        except AttributeError:
            pass

    def new(self, text):
        """visualizza il testo ricevuto da seriale attraveso il thread"""
        self.textOutput.appendPlainText(text)

class Monitor(QtCore.QThread):

    """classe che gestisce(!?) un thread per ricevere dati da seriale"""
    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)
        self.port = port
        self.baudrate = baudrate
        try: self.Connection = serial.Serial(self.port, self.baudrate)
        except serial.serialutil.SerialException:
            print "Impossibile collegarsi con la porta "+str(self.port)

    def run(self):
        """avvia un ciclo while che riceve dati da seriale"""
        while True:
            lChar = []
            while True:
                try:
                    char = self.Connection.read()
                    if ord(char) == 10:
                        break
                    else:
                        lChar.append(char)
                except serial.serialutil.SerialException:
                    char = str(self.port)+"\t"+str(self.baudrate)
                except AttributeError:
                    char = str(self.port)+"\t"+str(self.baudrate)
            self.emit(QtCore.SIGNAL("newText(QString)"), "".join(lChar[:-1]))
            time.sleep(0.001)
