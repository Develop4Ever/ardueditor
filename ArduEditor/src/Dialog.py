#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import expanduser # importo i moduli sys, time e serial
from PyQt4 import QtGui, QtCore # importo il toolkit grafico (PyQt4)

class newProject(QtGui.QDialog):

    def __init__(self, listProject):
        QtGui.QDialog.__init__(self)
        self.path = QtGui.QLineEdit()
        self.path.setReadOnly(True)
        listItem = [("New Project", expanduser("~"))]
        for i in listProject:
            listItem.append((i.split("/")[-1], i))
        self.choseBox = QtGui.QComboBox()
        self.choseBox.currentIndexChanged.connect(self.changePath)
        print listItem
        for x, y in listItem:
            self.choseBox.addItem(x+"-"+y)
        self.setWindowTitle('New Project/File')
        self.name = QtGui.QLineEdit()
        self.name.textChanged.connect(self.updatePath)
        self.name.returnPressed.connect(self.finish)
        self.finishButton = QtGui.QPushButton("Create!")
        self.finishButton.clicked.connect(self.finish)
        self.firstLine = QtGui.QHBoxLayout()
        self.firstLine.addWidget(QtGui.QLabel("Path: "))
        self.firstLine.addWidget(self.path)
        self.secondLine = QtGui.QHBoxLayout()
        self.secondLine.addWidget(QtGui.QLabel("Name: "))
        self.secondLine.addWidget(self.name)
        self.mainLayout = QtGui.QVBoxLayout(self)
        self.mainLayout.addWidget(self.choseBox)
        self.mainLayout.addLayout(self.firstLine)
        self.mainLayout.addLayout(self.secondLine)
        self.mainLayout.addWidget(self.finishButton)
        self.folder = expanduser("~")

    def updatePath(self, name):
        if self.folder == expanduser("~"):
            self.path.setText(self.folder+"/"+name+"/"+name+".ino")
        else:
            self.path.setText(self.folder+"/"+name+".ino")

    def changePath(self, index):
        name = self.choseBox.itemText(index).split("-")[-1]
        self.folder = name
        self.path.setText(name)

    def finish(self):
        self.close()

