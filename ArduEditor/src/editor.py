#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.Syntax import ArduinoHighlighter # importo il modulo relativo al syntax highlighter
from src.serialMonitor import SerialMonitor # importo il modulo relativo al SerialMonitor
from src.Dialog import newProject # importo la finestra di dialogo relativa ad un nuovo progetto
from PyQt4 import (QtGui, QtCore, QtWebKit) # importo il toolkit grafico (PyQt4)
import os, subprocess # importo il modulo os ed il modulo subprocess

#~ preferences = open("file/preferences.txt")
#~ listPreferences = []
#~ for i in preferences.readlines():
    #~ if '#' not in i:
        #~ i = i.split('=')
        #~ i = i.replace("\n", "")
        #~ i = i.replace(" ", "")
        #~ listPreferences.append(i)
#~ PROJECTPATH = listPreferences[0]
#~ BOARD = listPreferences[1]
#~ COM = listPreferences[2]

class newTextEditor:

    """crea una nuova tab contenente una TextEdit'"""
    def __init__(self, path=''):
        self.editor = QtGui.QWidget()
        self.path = path
        try:
            self.content = open(self.path, 'r')
            self.textContent = self.content.read()
            self.content.close()
        except IOError:
            self.textContent = ''
        self.usedFont = QtGui.QFont()
        self.usedFont.setFamily("DejaVuSansMono")
        self.usedFont.setPointSize(10)
        self.textEdit = QtGui.QPlainTextEdit()
        self.textEdit.setPlainText(self.textContent)
        self.textEdit.setFont(self.usedFont)
        self.logger = QtGui.QTextEdit()
        self.logger.setReadOnly(True)
        self.Syntax_HIGHLIGHT = ArduinoHighlighter(self.textEdit.document())
        self.splitView = QtGui.QSplitter(0x2)
        self.splitView.addWidget(self.textEdit)
        self.splitView.addWidget(self.logger)
        self.Layout = QtGui.QVBoxLayout(self.editor)
        self.Layout.addWidget(self.splitView)

class newBrowser():

    """ crea una nuova tab ed avvia la ricerca del testo selezionato"""
    def __init__(self, textToSearch):
        self.browser = QtGui.QWidget()
        self.webView = QtWebKit.QWebView()
        self.webView.load(QtCore.QUrl('http://www.google.com/search?q='+textToSearch))
        self.mainLayout = QtGui.QVBoxLayout(self.browser)
        self.mainLayout.addWidget(self.webView)

class Editor():

    """Classe principale, vengono create tutte le funzioni
       che consentono la creazione, la compilazione ed infine
       l' upload dello sketch per arduino"""

    def __init__(self):
        """crea la grafica e collega i vari widget alle relative finzioni"""
        self.mainWidget = QtGui.QWidget()
        self.menuFile = QtGui.QMenu("File")
        fileOption = [("New\t\t Ctrl+N", self.newFile, 'Ctrl+N'),
                      ("Open\t\t Ctrl+O", self.openFile, 'Ctrl+O'),
                      ("Close\t\t Ctrl+W", self.closeFile, 'Ctrl+W'),
                      ("Save\t\t Ctrl+S", self.saveFile, 'Ctrl+S'),
                      ("Save As\t\t ", self.saveFileAs, 'Ctrl+Alt+S')]
        for label, function, short in fileOption:
            self.menuFile.addAction(label, function, short)
        self.menuEdit = QtGui.QMenu("Edit")
        editOption = [("Undo\t\t Ctrl+Z", self.undoFile, "Ctrl+Z"),
                      ("Redo\t\t Ctrl+Y", self.redoFile, "Ctrl+Y"),
                      ("Cut\t\t Ctrl+X", self.cut, "Ctrl+X"),
                      ("Copy\t\t Ctrl+C", self.copy, "Ctrl+C"),
                      ("Paste\t\t Ctrl+P", self.paste, "Ctrl+P"),
                      ("Select All\t\t Ctrl+A", self.selectAll, "Ctrl+A"),
                      ("Comment\t\t Ctrl+/", self.comment, "Ctrl+7"),
                      ("Uncomment\t\t Ctrl+Alt+/", self.uncomment,"Ctrl+Alt+7"),
                      ("Increase Indent\t\t Ctrl+[", self.indentPP, "Ctrl+["),
                      ("Decrease Indent\t\t Ctrl+]", self.indentMM, "Ctrl+]"),
                      ("Search\t\t Ctrl+Maiusc+N", self.search, "Ctrl+Maiusc+M")]
        for label, function, shortcut in editOption:
            self.menuEdit.addAction(label, function, shortcut)
        self.menuSketch = QtGui.QMenu("Sketch")
        sketchOption = [("Verify/Compile\t\t Ctrl+R", self.compileFile, "Ctrl+R"),
                        ("Show Sketch Folder\t\t Ctrl+K", self.showFolder, "Ctrl+K"),
                        ("Upload\t\t Ctrl+U", self.uploadFile, "Ctrl+U")]
        for label, function, shortcut in sketchOption:
            self.menuSketch.addAction(label, function, shortcut)
        self.menuBar = QtGui.QMenuBar()
        self.menuBar.addMenu(self.menuFile)
        self.menuBar.addMenu(self.menuEdit)
        self.menuBar.addMenu(self.menuSketch)
        toolOption = [("images/New.png", "New", self.newFile),
                      ("images/Open.png", "Open", self.openFile),
                      ('images/Save.png', "Save", self.saveFile),
                      ('images/SaveAs.png',"SaveAs", self.saveFileAs),
                      ('images/Restore.png', "Restore", self.restoreFile),
                      ('images/Close.png', "Close", self.closeFile),
                      ('images/Back.png', "Back", self.undoFile),
                      ('images/Next.png', "Next", self.redoFile),
                      ('images/Make.png', "Make", self.compileFile),
                      ('images/Up.png', "Upload", self.uploadFile),
                      ('images/SMonitor.png', "SerialMonitor", self.openSMonitor)]
        self.toolBar = QtGui.QToolBar()
        for image, label, function in toolOption:
            if label == "Restore":
                self.toolBar.addSeparator()
            if label == "Back":
                self.toolBar.addSeparator()
            if label == "Make":
                self.toolBar.addSeparator()
            self.toolBar.addAction(QtGui.QIcon(image), label, function)
        self.tabWidget = QtGui.QTabWidget()
        self.tabWidget.setTabsClosable(True)
        self.tabWidget.tabCloseRequested.connect(self.closeFile)
        self.tabWidget.currentChanged.connect(self.verifyTab)
        self.mainLayout = QtGui.QVBoxLayout(self.mainWidget)
        self.mainLayout.addWidget(self.tabWidget)
        self.Instance = None
        self.BrowserInst = None
        self.listInstance = []

    def newFile(self):
        """crea un nuovo file nella Home"""
        listProject = []
        for i in self.listInstance:
            i = i.path
            listProject.append("/".join(str(i).split("/")[0:-1]))
        listProject = [x for x in set(listProject)]
        self.path = newProject(listProject)
        self.path.exec_()
        self.path = self.path.path.text()
        #print self.path
        self.name = self.path.split("/")[-1]
        #print self.name
        if self.path.split("/")[0:-1] not in listProject:
            self.folder = "/".join(str(self.path).split("/")[0:-1])
            if not os.path.exists(self.folder):
                os.makedirs(self.folder)
        #if ".ino" not in self.name:
            #self.name = self.name+".ino"
            #self.path = self.folder+"/"+self.name
            #print self.folder
        open(self.path, "w")
        self.Instance = newTextEditor(self.path)
        self.Instance.textEdit.setContextMenuPolicy(3)
        self.Instance.textEdit.customContextMenuRequested.connect(self.openMenu)
        self.listInstance.append(self.Instance)
        self.tabWidget.addTab(self.listInstance[-1].editor,
                              self.name)

    def openFile(self):
        """apre un file chiedendo all utente la path"""
        self.path = QtGui.QFileDialog.getOpenFileName()
        if self.path != '':
            self.Instance = newTextEditor(self.path)
            self.Instance.textEdit.setContextMenuPolicy(3)
            self.Instance.textEdit.customContextMenuRequested.connect(self.openMenu)
            self.listInstance.append(self.Instance)
            self.tabWidget.addTab(self.listInstance[-1].editor,
                                  self.path.split("/")[-1])

    def saveFile(self):
        """salva il file corrente nella sua path"""
        self.Index = self.tabWidget.currentIndex()
        self.path = self.listInstance[self.Index].path
        self.text = self.listInstance[self.Index].textEdit.toPlainText()
        self.save = open(self.path, "w")
        self.save.write(self.text)
        self.save.close()

    def saveFileAs(self):
        """salva il file corrente con una nuova path o con un nuovo nome"""
        self.Index = self.tabWidget.currentIndex()
        self.path = QtGui.QFileDialog.getSaveFileName()
        if self.path != '':
            self.text = self.listInstance[self.Index].textEdit.toPlainText()
            self.listInstance[self.Index].path = self.path
            self.save = open(self.path, "w")
            self.save.write(self.text)
            self.save.close()
            self.tabWidget.setTabText(self.Index, self.path.split("/")[-1])

    def restoreFile(self):
        """ricarica il testo direttamente dal file"""
        self.Index = self.tabWidget.currentIndex()
        self.path = self.listInstance[self.Index].path
        self.oldFile = open(self.path, "r")
        self.oldContent = self.oldFile.read()
        self.oldFile.close()
        self.listInstance[self.Index].textEdit.setPlainText(self.oldContent)

    def closeFile(self, Index=''):
        """chiude la tab ed il relativo file"""
        if Index == '':
            Index = self.tabWidget.currentIndex()
        del self.listInstance[Index]
        self.tabWidget.removeTab(Index)

    def undoFile(self):
        """annulla l' ultima operazione effettuata"""
        self.Index = self.tabWidget.currentIndex()
        self.listInstance[self.Index].textEdit.undo()

    def redoFile(self):
        """compie nuovamente l' ultima azione effettuata"""
        self.Index = self.tabWidget.currentIndex()
        self.listInstance[self.Index].textEdit.redo()

    def uploadFile(self):
        """TODO: dovrebbe compilare e uppare"""
        msg = "TODO: line 176 of ArduEditor/src/editor.py for more info"
        raise NotImplementedError, msg

    def compileFile(self):
        """TODO: dovrebbe compilare e uppare"""
        msg = "TODO: line 181 of ArduEditor/src/editor.py for more info"
        raise NotImplementedError, msg

    def showFolder(self):
        """TODO: dovrebbe visualizzare la directory contenente il progetto"""
        msg = "TODO: line 195 of ArduEditor/src/editor.py for more info"
        raise NotImplementedError, msg

    def verifyTab(self, index):
        """verifica se la tab corrente e' relativa al browser e nasconde la toolbar"""
        if self.listInstance[index] == self.BrowserInst:
            self.toolBar.hide()
        else:
            self.toolBar.show()

    def copy(self):
        """copia il testo selezionato"""
        self.Index = self.tabWidget.currentIndex()
        self.listInstance[self.Index].textEdit.copy()

    def cut(self):
        """taglia il testo selezionato"""
        self.Index = self.tabWidget.currentIndex()
        self.listInstance[self.Index].textEdit.cut()

    def paste(self):
        """incolla il testo precedentemente copiato/tagliato"""
        self.Index = self.tabWidget.currentIndex()
        self.listInstance[self.Index].textEdit.paste()

    def selectAll(self):
        self.Index = self.tabWidget.currentIndex()
        self.listInstance[self.Index].textEdit.selectAll()

    def comment(self):
        self.addPrefix("//")

    def uncomment(self):
        self.rmPrefix("//")

    def indentPP(self):
        self.addPrefix(" "*4)

    def indentMM(self):
        self.rmPrefix(" "*4)

    def addPrefix(self, prefix):
        """aggiunge il carattere passatogli come parametro ad inizio della riga"""
        self.Index = self.tabWidget.currentIndex()
        cursor = self.listInstance[self.Index].textEdit.textCursor()
        if cursor.selectedText().isEmpty():
            cursor.movePosition(QtGui.QTextCursor.StartOfBlock)
            cursor.insertText(prefix)
        else:
            startPosition = cursor.selectionStart()
            endPosition = cursor.selectionEnd()
            cursor.setPosition(endPosition)
            cursor.movePosition(QtGui.QTextCursor.StartOfBlock)
            print startPosition
            while cursor.position() > startPosition:
                print cursor.position()
                cursor.insertText(prefix)
                cursor.movePosition(QtGui.QTextCursor.PreviousBlock)
                cursor.movePosition(QtGui.QTextCursor.StartOfBlock)
                if cursor.position() == 0:
                    break
            cursor.insertText(prefix)

    def rmPrefix(self, prefix):
        """verifica l esistenza del carattere ad inizio riga e lo elimina"""
        self.Index = self.tabWidget.currentIndex()
        cursor = self.listInstance[self.Index].textEdit.textCursor()
        if cursor.selectedText().isEmpty():
            cursor.movePosition(QtGui.QTextCursor.StartOfBlock)
            cursor.setPosition(cursor.position()+len(prefix),
                               QtGui.QTextCursor.KeepAnchor)
            if unicode(cursor.selectedText()) == prefix:
                cursor.removeSelectedText()
        else:
            startPosition = cursor.selectionStart()
            endPosition = cursor.selectionEnd()
            cursor.setPosition(endPosition)
            cursor.movePosition(QtGui.QTextCursor.StartOfBlock,
                                QtGui.QTextCursor.KeepAnchor)
            oldPosition = None
            while cursor.position() >= startPosition:
                newPosition = cursor.position()
                if oldPosition == newPosition:
                    break
                else:
                    oldPosition = newPosition
                cursor.movePosition(QtGui.QTextCursor.StartOfBlock)
                cursor.setPosition(cursor.position()+len(prefix),
                                   QtGui.QTextCursor.KeepAnchor)
                if unicode(cursor.selectedText()) == prefix:
                    cursor.removeSelectedText()
                cursor.movePosition(QtGui.QTextCursor.PreviousBlock)
                cursor.movePosition(QtGui.QTextCursor.EndOfBlock)

    def search(self):
        """avvia il browser cercando la parola/testo selezionata/o"""
        self.Index = self.tabWidget.currentIndex()
        self.cursor = self.listInstance[self.Index].textEdit.textCursor()
        self.textToSearch = self.cursor.selection().toPlainText()
        self.BrowserInst = newBrowser(self.textToSearch)
        self.listInstance.append(self.BrowserInst)
        self.tabWidget.addTab(self.listInstance[-1].browser, "Search")

    def openSMonitor(self):
        """apre il Serial Monitor in una nuova finestra"""
        self.newSMonitor = SerialMonitor()
        self.newSMonitor.exec_()

    def openMenu(self, point):
        """visualizza il menu nel punto ove viene cliccato col tasto dx"""
        self.menuEdit.exec_(self.Instance.textEdit.mapToGlobal(point))
