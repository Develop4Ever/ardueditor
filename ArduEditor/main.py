#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt4 import QtGui, QtCore # importo il toolkit grafico (PyQt4)
from src.editor import Editor # importo l' Editor
import sys # importo il modulo sys

class PyIDE(QtGui.QMainWindow):

    """classe principale, crea una finestra, posiziona la ToolBar
       il MenuBar ed il Widget centrale ed avvia il mainloop"""
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.Editor = Editor()
        self.resize(500, 500)
        self.setWindowTitle('PyIDE')
        self.setMenuBar(self.Editor.menuBar)
        self.addToolBar(self.Editor.toolBar)
        self.setCentralWidget(self.Editor.mainWidget)


app = QtGui.QApplication(sys.argv)
main = PyIDE()
main.show()
app.exec_()
